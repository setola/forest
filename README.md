# Scenario

Una foresta si snoda su un terreno rettangolare di m per n metri.

All’interno di questo rettangolo gli alberi sono posti in maniera tale da
trovarsi in coordinate corrispondenti a valori interi sia in ascissa che in ordinata.

Inoltre, per un bizzarro fenomeno naturale, non esiste più di un albero su
una stessa ascissa o su una stessa ordinata.

Ogni qualvolta si desiderano tagliare degli alberi, si identifica l’area da
disboscare, ovvero un rettangolo ai cui vertici contrapposti si trovano 2 alberi.

Successivamente si abbattono tutti gli alberi all’interno di tale rettangolo
(inclusi i 2 vertici).


## Esercizio 1

Quello che ti chiediamo è di definire ciò che ritieni necessario
(funzioni, classi e metodi) per rappresentare la situazione descritta nello scenario.

Non è necessario implementare tutte le funzioni/metodi, ma è sufficiente
definirne le firme. Nulla ti vieta però di realizzare qualcosa di funzionante :)

## Esercizio 2

In questo secondo esercizio ti chiediamo di implementare
(se non l’hai già fatto nell’esercizio 1) l’algoritmo di scoperta delle
aree da disboscare. Tale algoritmo deve, dati una foresta F (popolata)
e un intero K, individuare tutte le possibili aree (se ne esistono)
contenenti esattamente K alberi (vertici inclusi).

Puoi scriverlo in pseudocodice o in alternativa nel linguaggio che ritieni
più opportuno (preferibilmente PHP o Javascript).

## Esercizio 3

Supponiamo che esista un web service

http://www.awesomesite.com/forest/discover/F/K

che, se interrogato usando F (ID di una foresta esistente) e K (numero di
alberi da tagliare) come parametri, restituisca un JSON cosi strutturato:

```
[
    [ [V1x,V1y] , [V2x,V2y ] ] , /* area 1 */
    [ [V1x,V1y] , [V2x,V2y ] ] , /* area 2 */
    …
    [ [V1x,V1y] , [V2x,V2y ] ] , /* area N */
]
```

ove V1x = coordinata x del primo vertice,
V2y = coordinata y del secondo vertice, e così via.

Ti chiediamo di realizzare una pagina html (http://www.awesomesite.com/home.html)
attraverso la quale un utente sia in grado di inserire i dati di input
( F , K ) e interrogare il servizio, visualizzandone poi la relativa risposta.

Puoi usare i linguaggi e/o le librerie che preferisci.

Buon Lavoro!