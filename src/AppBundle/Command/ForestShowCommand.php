<?php

namespace AppBundle\Command;

use AppBundle\Entity\Forest;
use AppBundle\Entity\Tree;
use AppBundle\Service\ForestManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ForestShowCommand extends ContainerAwareCommand {

    const ARG_FOREST_ID = 'forestID';
    const MARKUP_TREE   = '*';
    const MARKUP_VOID   = ' ';
    const MARKUP_CUT    = 'X';

    protected function configure() {
        $this
            ->setName('forest:show')
            ->setDescription('show a matrix for the given forest')
            ->addArgument(static::ARG_FOREST_ID, InputArgument::REQUIRED, 'ID of the forest')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()
            ->get('doctrine.orm.entity_manager')
        ;

        /** @var Forest $forest */
        $forest = $entityManager->getRepository('AppBundle:Forest')
            ->find($input->getArgument(static::ARG_FOREST_ID))
        ;

        /** @var ForestManager $forestManager */
        $forestManager = $this->getContainer()
            ->get(ForestManager::class)
        ;

        $matrix = $forestManager::toMatrix($forest);

        foreach ($matrix as $row) {
            foreach ($row as $col) {
                if (false === $col) {
                    $output->write(static::MARKUP_VOID);
                } else {
                    $output->write($col == Tree::STATUS_ALIVE ? static::MARKUP_TREE : static::MARKUP_CUT);
                }
            }
            $output->writeln('');
        }

        $output->writeln('======================');
        $output->writeln(
            sprintf(
                'Forest %d - %d trees (%01.2f%%) - %dx%d',
                $forest->getId(),
                $forest->countTrees(),
                $forest->percentage(),
                $forest->getWidth(),
                $forest->getHeight()
            )
        );
    }

}
