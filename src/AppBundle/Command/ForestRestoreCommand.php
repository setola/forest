<?php

namespace AppBundle\Command;

use AppBundle\Entity\Forest;
use AppBundle\Entity\Tree;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ForestRestoreCommand extends ContainerAwareCommand {

    const ARG_FOREST_ID = 'forestID';

    protected function configure() {
        $this
            ->setName('forest:restore')
            ->setDescription('Restores all trees to alive status')
            ->addArgument(static::ARG_FOREST_ID, InputArgument::REQUIRED, 'ID of the forest')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()
            ->get('doctrine.orm.entity_manager')
        ;

        /** @var Forest $forest */
        $forest = $entityManager->getRepository('AppBundle:Forest')
            ->find($input->getArgument(static::ARG_FOREST_ID))
        ;

        foreach ($forest->getTrees() as $tree) {
            $tree->setStatus(Tree::STATUS_ALIVE);
        }

        $entityManager->persist($forest);
        $entityManager->flush();

        $output->writeln(sprintf('Forest %d revived :)', $forest->getId()));
    }

}
