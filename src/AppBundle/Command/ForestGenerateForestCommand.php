<?php

namespace AppBundle\Command;

use AppBundle\Service\ForestManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ForestGenerateForestCommand extends ContainerAwareCommand {

    const ARG_WIDTH  = 'width';
    const ARG_HEIGHT = 'height';

    protected function configure() {
        $this
            ->setName('forest:generate:forest')
            ->setDescription('Generates a random forest')
            ->addArgument(static::ARG_WIDTH, InputArgument::OPTIONAL, 'Forest width', 100)
            ->addArgument(static::ARG_HEIGHT, InputArgument::OPTIONAL, 'Forest height', 100)
            //->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln(
            sprintf(
                'Generating new forest (%sx%s)',
                $input->getArgument(static::ARG_WIDTH),
                $input->getArgument(static::ARG_HEIGHT)
            )
        );

        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()
            ->get('doctrine.orm.entity_manager')
        ;

        /** @var ForestManager $forestManager */
        $forestManager = $this->getContainer()
            ->get(ForestManager::class)
        ;

        $forest = $forestManager::generateForest(
            $input->getArgument(static::ARG_WIDTH),
            $input->getArgument(static::ARG_HEIGHT)
        );

        $entityManager->persist($forest);
        $entityManager->flush();

        $output->writeln(
            sprintf(
                'Generated Forest %d - %d trees (%01.2f%%) - %dx%d',
                $forest->getId(),
                $forest->countTrees(),
                $forest->percentage(),
                $forest->getWidth(),
                $forest->getHeight()
            )
        );
    }

}
