<?php

namespace AppBundle\Command;

use AppBundle\Entity\Forest;
use AppBundle\Entity\Tree;
use AppBundle\Service\ForestManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ForestCutCommand extends ContainerAwareCommand {

    const ARG_FOREST_ID = 'forestID';
    const ARG_FROM      = 'from';
    const ARG_TO        = 'to';

    protected function configure() {
        $this
            ->setName('forest:cut')
            ->setDescription('Cuts an area between the two given trees')
            ->addArgument(static::ARG_FOREST_ID, InputArgument::REQUIRED, 'ID of the forest')
            ->addArgument(static::ARG_FROM, InputArgument::REQUIRED, 'Tree index for the initial cut point')
            ->addArgument(static::ARG_TO, InputArgument::REQUIRED, 'Tree index for the last cut point')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()
            ->get('doctrine.orm.entity_manager')
        ;

        /** @var Forest $forest */
        $forest = $entityManager->getRepository('AppBundle:Forest')
            ->find($input->getArgument(static::ARG_FOREST_ID))
        ;

        /** @var ForestManager $forestManager */
        $forestManager = $this->getContainer()
            ->get(ForestManager::class)
        ;

        $from = $forest->getTree($input->getArgument(static::ARG_FROM))
            ->getCoordinates()
        ;
        $to   = $forest->getTree($input->getArgument(static::ARG_TO))
            ->getCoordinates()
        ;

        $cuts = $forestManager::cut($forest, $from, $to);

        foreach ($cuts as $cut) {
            $tree        = $forest->getTree($cut);
            $coordinates = $tree->getCoordinates();
            $output->writeln(
                sprintf(
                    'Cutting tree %d (%d,%d)',
                    $tree->getId(),
                    $coordinates->getX(),
                    $coordinates->getY()
                )
            );
        }


        $entityManager->persist($forest);
        $entityManager->flush();

        $output->writeln(
            sprintf(
                'Cut %d trees from: (%d,%d) to (%d,%d)',
                count($cuts),
                $from->getX(),
                $from->getY(),
                $to->getX(),
                $to->getY()
            )
        );
    }

}
