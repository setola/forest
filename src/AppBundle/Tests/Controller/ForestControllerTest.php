<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ForestControllerTest extends WebTestCase
{
    public function testGettrees()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'trees');
    }

    public function testCuttrees()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'trees/cut');
    }

    public function testGettree()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'tree/{id}');
    }

}
