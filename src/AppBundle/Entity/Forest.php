<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Forest
 *
 * @ORM\Table(name="forest")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ForestRepository")
 */
class Forest {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="width", type="integer")
     */
    private $width;

    /**
     * @var int
     *
     * @ORM\Column(name="height", type="integer")
     */
    private $height;

    /**
     * @var Tree[]
     *
     * @ORM\OneToMany(targetEntity="Tree", mappedBy="forest", cascade={"persist"})
     * @ORM\JoinColumn(name="trees", referencedColumnName="id")
     */
    private $trees;


    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return Forest
     */
    public function setWidth($width) {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return int
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     * @return Forest
     */
    public function setHeight($height) {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return int
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * Get trees
     *
     * @return Tree[]
     */
    public function getTrees() {
        return $this->trees;
    }

    /**
     * Set trees
     *
     * @param Tree[] $trees
     * @return Forest
     */
    public function setTrees(array $trees) {
        $this->trees = $trees;

        return $this;
    }

    public function addTree(Tree $tree) {
        $this->trees[] = $tree;

        return $this;
    }

    public function getTree($index) {
        return isset($this->trees[$index]) ? $this->trees[$index] : false;
    }

    public function countTrees() {
        return count($this->getTrees());
    }

    public function percentage() {
        return $this->countTrees() / ($this->getWidth() * $this->getHeight()) * 100;
    }
}

