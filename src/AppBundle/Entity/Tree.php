<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tree
 *
 * @ORM\Table(name="tree")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TreeRepository")
 */
class Tree {

    const STATUS_ALIVE = 10;
    const STATUS_CUT   = 99;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Forest
     *
     * @ORM\ManyToOne(targetEntity="Forest", inversedBy="trees", cascade={"persist"})
     * @ORM\JoinColumn(name="forest", referencedColumnName="id")
     */
    private $forest;

    /**
     * @var Coordinates
     *
     * @ORM\OneToOne(targetEntity="Coordinates", mappedBy="id", cascade={"persist"})
     * @ORM\JoinColumn(name="coordinates", referencedColumnName="id")
     */
    private $coordinates;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the forest
     *
     * @return Forest
     */
    public function getForest() {
        return $this->forest;
    }

    /**
     * Set the forest
     *
     * @param Forest $forest
     *
     * @return Tree
     */
    public function setForest(Forest $forest) {
        $this->forest = $forest;

        return $this;
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates() {
        return $this->coordinates;
    }

    /**
     * @param Coordinates $coordinates
     * @return Tree
     */
    public function setCoordinates(Coordinates $coordinates) {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Tree
     * @throws \Exception
     */
    public function setStatus($status) {
        switch ($status) {
            case static::STATUS_ALIVE:
            case static::STATUS_CUT:
                $this->status = $status;
                break;
            default:
                throw new \Exception('Invalid Status: ' . $status);
        }

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }
}

