<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coordinates
 *
 * @ORM\Table(name="coordinates")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CoordinatesRepository")
 */
class Coordinates {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="x", type="integer", nullable=true)
     */
    private $x;

    /**
     * @var int
     *
     * @ORM\Column(name="y", type="integer", nullable=true)
     */
    private $y;


    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set x
     *
     * @param integer $x
     *
     * @return Coordinates
     */
    public function setX($x) {
        $this->x = intval($x);

        return $this;
    }

    /**
     * Get x
     *
     * @return int
     */
    public function getX() {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param integer $y
     *
     * @return Coordinates
     */
    public function setY($y) {
        $this->y = intval($y);

        return $this;
    }

    /**
     * Get y
     *
     * @return int
     */
    public function getY() {
        return $this->y;
    }
}

