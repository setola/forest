<?php
/**
 * Created by PhpStorm.
 * User: setola
 * Date: 15/06/17
 * Time: 15.12
 */

namespace AppBundle\Service;


use AppBundle\Entity\Coordinates;
use AppBundle\Entity\Forest;
use AppBundle\Entity\Tree;

/**
 * Class ForestManager
 * @package AppBundle\Service
 */
class ForestManager {

    public static function generateForest($width, $height) {
        $forest = new Forest();
        $forest
            ->setWidth($width)
            ->setHeight($height)
        ;

        return static::linearXRandomYGenerator($forest);
    }

    protected static function linearGenerator(Forest $forest) {
        $width  = $forest->getWidth();
        $height = $forest->getHeight();

        $limits = ['x' => [], 'y' => []];

        for ($x = 1; $x < $width; $x++) {
            if (in_array($x, $limits['x'])) continue;
            for ($y = 1; $y < $height; $y++) {
                if (in_array($x, $limits['x'])) continue;
                if (in_array($y, $limits['y'])) continue;
                if ((rand(0, 10) % 2) === 0) {
                    $tree = new Tree();

                    $coordinates = new Coordinates();
                    $coordinates
                        ->setX($x)
                        ->setY($y)
                    ;
                    $tree
                        ->setStatus(Tree::STATUS_ALIVE)
                        ->setForest($forest)
                        ->setCoordinates($coordinates)
                    ;

                    $forest->addTree($tree);
                    $limits['x'][] = $x;
                    $limits['y'][] = $y;
                }
            }
        }

        return $forest;
    }

    protected static function linearXRandomYGenerator(Forest $forest) {
        $width  = $forest->getWidth();
        $height = $forest->getHeight();

        $limits = ['x' => [], 'y' => []];

        for ($x = 1; $x < $width; $x++) {
            $y = rand(0, $height);
            if (in_array($y, $limits['y'])) continue;

            $tree = new Tree();

            $coordinates = new Coordinates();
            $coordinates
                ->setX($x)
                ->setY($y)
            ;
            $tree
                ->setStatus(Tree::STATUS_ALIVE)
                ->setForest($forest)
                ->setCoordinates($coordinates)
            ;

            $forest->addTree($tree);
            $limits['x'][] = $x;
            $limits['y'][] = $y;
        }

        return $forest;
    }

    public static function matrixInit($width, $height) {
        $matrix = [];

        for ($x = 0; $x < $width; $x++) {
            $row = [];
            for ($y = 0; $y < $height; $y++) {
                $row[] = false;
            }
            $matrix[] = $row;
        }

        return $matrix;
    }

    public static function toMatrix(Forest $forest) {
        $matrix = static::matrixInit($forest->getWidth(), $forest->getHeight());
        foreach ($forest->getTrees() as $tree) {
            $coordinates = $tree->getCoordinates();

            $matrix[$coordinates->getX()][$coordinates->getY()] = $tree->getStatus();
        }

        return $matrix;
    }

    public static function cut(Forest $forest, Coordinates $from, Coordinates $to) {

        $cuts = [];

        foreach ($forest->getTrees() as $tree) {
            if (static::treeIsBetweenRectangularBounds($tree, $from, $to)) {
                $tree->setStatus(Tree::STATUS_CUT);
                $cuts[] = $tree->getId();
            }
        }

        return $cuts;
    }

    public static function treeIsBetweenRectangularBounds(Tree $tree, Coordinates $from, Coordinates $to) {
        $coords = $tree->getCoordinates();

        $bounds = [
            'x' => ['min' => min($from->getX(), $to->getX()), 'max' => max($from->getX(), $to->getX())],
            'y' => ['min' => min($from->getY(), $to->getY()), 'max' => max($from->getY(), $to->getY())],
        ];

        return (
            $coords->getX() >= $bounds['x']['min']
            && $coords->getX() <= $bounds['x']['max']
            && $coords->getY() >= $bounds['y']['min']
            && $coords->getY() <= $bounds['y']['max']
        );
    }
}