<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Forest;
use AppBundle\Service\ForestManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ForestController extends Controller {

    /**
     * @Route("/forest/{width}/{height}", requirements={"width": "\d+", "height": "\d+"}, defaults={"width" = 100, "height": 100})
     * @Method({"POST"})
     *
     * @param $width
     * @param $height
     * @param EntityManagerInterface $entityManager
     * @param ForestManager $forestManager @
     * @return JsonResponse
     */
    public function generateForestAction($width, $height, EntityManagerInterface $entityManager, ForestManager $forestManager) {
        $forest = $forestManager::generateForest($width, $height);
        $entityManager->persist($forest);
        $entityManager->flush();

        return new JsonResponse(['data' => ['forestId' => $forest->getId()]]);
    }

    /**
     * @Route("/forest/{forestID}", requirements={"forestID": "\d+"})
     * @Method({"GET"})
     *
     * @param $forestID
     * @param EntityManagerInterface $entityManager
     * @param ForestManager $forestManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTreesMatrixAction($forestID, EntityManagerInterface $entityManager, ForestManager $forestManager) {
        /** @var Forest $forest */
        $forest = $entityManager->getRepository('AppBundle:Forest')
            ->find($forestID)
        ;

        return new JsonResponse(['data' => $forestManager::toMatrix($forest)]);
    }

    /**
     * @Route("/forest/{forestID}/cut/{from}/{to}", requirements={"forestID": "\d+", "from": "\d+", "to": "\d+"})
     * @Method({"POST"})
     *
     * @param $forestID
     * @param $from
     * @param $to
     * @param EntityManagerInterface $entityManager
     * @param ForestManager $forestManager
     * @return JsonResponse
     */
    public function cutTreesAction($forestID, $from, $to, EntityManagerInterface $entityManager, ForestManager $forestManager) {
        /** @var Forest $forest */
        $forest = $entityManager->getRepository('AppBundle:Forest')
            ->find($forestID)
        ;

        $from = $forest->getTree($from)
            ->getCoordinates()
        ;
        $to   = $forest->getTree($to)
            ->getCoordinates()
        ;

        $cuts = $forestManager::cut($forest, $from, $to);

        return new JsonResponse(['data' => $cuts]);
    }

    /**
     * @Route("/forest/{forestID}/tree/{treeID}", requirements={"forestID": "\d+", "treeID": "\d+"})
     * @Method({"GET"})
     *
     * @param $forestID
     * @param $treeID
     * @param EntityManagerInterface $entityManager
     * @param ForestManager $forestManager
     * @return JsonResponse
     */
    public function getTreeAction($forestID, $treeID, EntityManagerInterface $entityManager, ForestManager $forestManager) {
        /** @var Forest $forest */
        $forest = $entityManager->getRepository('AppBundle:Forest')
            ->find($forestID)
        ;

        $tree        = $forest->getTree($treeID);
        $coordinates = $tree->getCoordinates();

        return new JsonResponse(
            [
                'data' =>
                    [
                        'id'          => $tree->getId(),
                        'forest'      => $forest->getId(),
                        'status'      => $tree->getStatus(),
                        'coordinates' => [
                            $coordinates->getX(),
                            $coordinates->getY(),
                        ],
                    ],
            ]
        );
    }

    /**
     * @Route("/forest/{forestID}/area/{numberOfTrees}", requirements={"forestID": "\d+"})
     * @Method({"GET"})
     * @param $forestID
     * @param $numberOfTrees
     * @param EntityManagerInterface $entityManager
     * @param ForestManager $forestManager
     * @return JsonResponse
     * TODO: remove the mocked service :)
     */
    public function getCuttingAreas($forestID, $numberOfTrees, EntityManagerInterface $entityManager, ForestManager $forestManager) {
        /** @var Forest $forest */
        $forest = $entityManager->getRepository('AppBundle:Forest')
            ->find($forestID)
        ;
        return new JsonResponse([[1, 3], [5, 7]], [[2, 5], [9, 3]]);
    }

}
