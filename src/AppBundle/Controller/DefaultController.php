<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, EntityManagerInterface $entityManager) {
        return $this->render(
            'AppBundle:Forest:index.html.twig',
            [
                'forests'  => $entityManager->getRepository('AppBundle:Forest')
                    ->count(),
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            ]
        );
    }

    /**
     * @Route("/test", name="test")
     * @param Request $request
     * @return JsonResponse
     */
    public function testAction(Request $request) {
        return new JsonResponse(['result' => 'ok']);
    }
}
